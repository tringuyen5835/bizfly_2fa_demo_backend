import requests
import secrets
from flask import Flask, request, jsonify, make_response, abort
from flask_cors import CORS
from config import ApiConfig

from .utils import validate_email, validate_phone

app = Flask(import_name='Demo - Bizfly 2FA')
app.config.from_object(ApiConfig)

CORS(app,
     supports_credentials=True,
     automatic_options=True)


def _send_otp(otp_session_uid, delivery_type='sms'):
    end_point = ApiConfig.BIZFLY_2FA_URL + '/otp/send'
    payload = dict(
        otp_session_uid=otp_session_uid,
        delivery_type=delivery_type
    )

    res = requests.post(end_point, json=payload)
    return res


@app.route('/otp/generate-session', methods=['POST'])
def generate_session():
    params = request.json

    email = params.get('email')
    phone = params.get('phone')
    popup_enabled = params.get('popup_enabled')

    if not email or not validate_email(email):
        return abort(400)

    if not phone or not validate_phone(phone):
        return abort(400)

    payload = dict(
        email=email,
        phone=phone,
        app_secret=ApiConfig.BIZFLY_2FA_SECRET_KEY
    )
    end_point = ApiConfig.BIZFLY_2FA_URL + '/otp/generate-session'

    res = requests.post(end_point, json=payload)
    if res.status_code == 200:
        res_data = res.json()
        otp_session_uid = res_data['session_uid']

        # send otp to phone if not using popup
        if not popup_enabled:
            _send_otp(otp_session_uid,
                      delivery_type='sms')
        return make_response(jsonify({'otp_session_uid': otp_session_uid}))

    # error
    return abort(400)


@app.route('/otp/verify', methods=['POST'])
def verify_otp():
    params = request.json
    otp = params.get('otp')
    otp_session_uid = params.get('otp_session_uid')

    if not otp or not otp_session_uid:
        return abort(400)

    payload = dict(
        otp_session_uid=otp_session_uid,
        otp=otp
    )

    end_point = ApiConfig.BIZFLY_2FA_URL + '/otp/verify'

    res = requests.post(end_point, json=payload)

    if res.status_code >= 500:
        return make_response(jsonify({'message': res.content}), 500)

    data = res.json()

    if res.status_code >= 400:
        # error
        error_code = data['error']

        message = 'Có lỗi xảy ra.'

        if error_code == 'InvalidOTP':
            message = 'OTP không hợp lệ'
        elif error_code == 'InvalidSessionUID':
            message = 'Otp session không hợp lệ hoặc đã hết hạn'
        return make_response(jsonify({'message': message}), 400)

    if not data.get('success'):
        return make_response(jsonify({'message': 'OTP không hợp lệ'}), 400)

    response_data = dict(
        # This is your awesome access_token
        access_token=secrets.token_urlsafe(),
        user_info=dict(
            name='Username',
            some_extra_info='Extra info?'
        )
    )
    return make_response(jsonify(response_data))



@app.route('/otp/send', methods=['POST'])
def send_otp():
    params = request.json

    otp_session_uid = params.get('otp_session_uid')
    delivery_type = params.get('delivery_type')

    if not otp_session_uid:
        return abort(400)

    res = _send_otp(otp_session_uid, delivery_type)
    if res.status_code == 200:
        return make_response(jsonify({'success': True}))

    # error
    error_data = res.json()
    error_code = error_data['error']

    message = 'Có lỗi xảy ra.'

    if error_code == 'SendingIntervalError':
        message = 'Xin hãy đợi thêm giây lát. Bạn đã vừa gửi OTP rồi.'
    elif error_code == 'InvalidSessionUID':
        message = 'Otp session không hợp lệ hoặc đã hết hạn'
    return make_response(jsonify({'message': message}), 400)


# this route is only for popup using,
# you need to confirm that the otp_session_uid is really verified from the popup
@app.route('/otp/confirm-session', methods=['POST'])
def confirm_session():
    params = request.json
    otp_session_uid = params.get('otp_session_uid')
    email = params.get('email')

    if not otp_session_uid or not email or not validate_email(email):
        return abort(400)

    payload = dict(
        otp_session_uid=otp_session_uid,
        email=email
    )

    end_point = ApiConfig.BIZFLY_2FA_URL + '/otp/session/confirm'

    res = requests.post(end_point, json=payload)

    if res.status_code == 200:
        response_data = dict(
            # This is your awesome access_token
            access_token=secrets.token_urlsafe(),
            user_info=dict(
                name='Username',
                some_extra_info='Extra info?'
            )
        )
        return make_response(jsonify(response_data))

    # error
    return abort(400)
