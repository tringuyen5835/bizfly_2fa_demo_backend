import os


class BaseConfig(object):
    APP_NAME = '2FA - DEMO'

    ENV_NAME = os.getenv('ENV_NAME', 'local')

    SECRET_KEY = os.getenv('SECRET_KEY', 'This is a very secret key')

    STATIC_FOLDER = 'static'


class ApiConfig(BaseConfig):
    REQUEST_MAX_SIZE = 15 * 1024 * 1024

    DEBUG = True

    BIZFLY_2FA_SECRET_KEY = os.getenv('OTP_SECRET_KEY', 'Your app secret key')
    BIZFLY_2FA_URL = os.getenv('BIZFLY_2FA_URL', 'https://dev.bizflycloud.vn/twofa/api/public')

