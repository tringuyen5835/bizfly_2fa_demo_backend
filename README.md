# Bizfly 2FA Demo

## Requirement
- Ubuntu 18.04
- Python 3.6+

## Setup
```pip3 install pipenv```

```git clone https://gitlab.com/tringuyen5835/bizfly_2fa_demo_backend.git```

```cd bizfly_2fa_demo_backend```

```pipenv install```

## Configuration
```export OTP_SECRET_KEY=your bizfly 2fa secret key```

```export BIZFLY_2FA_URL=https://dev.bizflycloud.vn/twofa/api/public```

```export SECRET_KEY=your demo secret key```

## Run
```pipenv shell```

```uwsgi --socket=:5000 --protocol=http --wsgi-file=run.py --callable=app```
